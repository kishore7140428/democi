#!/bin/bash

# Convert line endings
dos2unix deploy_script.sh

# Execute the main script
bash deploy_script.sh